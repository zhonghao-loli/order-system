package com.order.system.orderService.service.impl;

import com.order.system.common.result.ServiceResult;
import com.order.system.orderService.common.query.PaymentRequest;
import com.order.system.orderService.common.query.PlaceOrderRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author: zhonghao
 * @date: 2019/01/28 10:07:34
 * @description: OrderInfo服务实现
 */
@Service
@Transactional
public class OrderService {


    public ServiceResult placeOrder(PlaceOrderRequest request) {
        return null;
    }


    public ServiceResult confirmation(PaymentRequest request) {
        return null;
    }
}
