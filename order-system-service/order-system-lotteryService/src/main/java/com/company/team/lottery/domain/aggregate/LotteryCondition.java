package com.company.team.lottery.domain.aggregate;

import com.company.team.lottery.domain.entity.ActivityCondition;
import com.company.team.lottery.domain.entity.LotteryChance;
import com.company.team.lottery.domain.valobj.DrawLotterContext;
import com.company.team.lottery.facade.UserInfoFacade;
import com.order.system.common.VO.UserInfoVO;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * 抽奖抽奖限制聚合根
 * @author zh
 * @date  2021-01-28
 */
public class LotteryCondition {
    @Autowired
    private UserInfoFacade userInfoFacade;

    @Autowired
    private ActivityCondition activityCondition;

    @Autowired
    private LotteryChance lotteryChance;

    /**
     * 主要方法：校验活动权限
     */
    public void checkLotteryLimit(DrawLotterContext dlc){
        // 获取用户信息
        UserInfoVO userInfo = userInfoFacade.getUserInfoByUserId(dlc);

        // 获取本次用户抽奖的机会
        LotteryChance lotteryChance = this.lotteryChance.getLotteryChance(dlc.getUserId(), dlc.getChanceSoucre(), dlc.getLotteryId());

        // 校验是否有资格
        activityCondition.checkActivityCondition();

        // 将本次机会消除
        lotteryChance.updateLotteryChance(lotteryChance.getLotteryChanceId());
    }
}
