package com.company.team.lottery.repo;

import com.company.team.lottery.domain.entity.Prize;
import com.company.team.lottery.repo.dao.PrizeMapper;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

@Component
public class PrizeRepository {

    @Autowired
    private PrizeMapper prizeMapper;

    // 用于模仿NoSql操作，redis,Guava,mongoDB等
    private Map<Long,List<Prize>> prizeCacheAccessObj = Maps.newConcurrentMap();


    public List<Prize> getPrizeByLotteryId(Long lotteryId){
        List<Prize> prizes = prizeCacheAccessObj.get(lotteryId);

        if(CollectionUtils.isEmpty(prizes)){
            List<Prize> prizesFromDB = prizeMapper.selectByIds(lotteryId.toString());
            prizeCacheAccessObj.put(lotteryId,prizesFromDB);
            return prizesFromDB;
        }
        return prizes;
    }


}
