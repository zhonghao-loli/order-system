package com.company.team.lottery.service.impl;

import com.company.team.lottery.domain.valobj.DrawLotterContext;
import com.order.system.common.result.ServiceResult;

public interface ILotteryService {

    ServiceResult participateLottery(DrawLotterContext dlc);

}
