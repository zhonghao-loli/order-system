package com.company.team.lottery.domain.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 *  活动限制条件实体
 */
@Component
@Data
public class ActivityCondition {

    private Long activityId;

    private Date startDate;

    private Date endDate;

    /**限制规定人群*/
    private String userGroup;

    public void checkActivityCondition(){
        return;
    }

}
