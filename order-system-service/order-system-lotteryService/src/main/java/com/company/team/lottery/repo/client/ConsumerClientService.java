package com.company.team.lottery.repo.client;

import com.order.system.common.result.ServiceResult;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient("consumer")
public interface ConsumerClientService {
    @RequestMapping(method = RequestMethod.POST, value = "/user/getUserInfoByUserId.do", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    ServiceResult getUserInfo(@RequestParam("userId") final Long userId);

}
