package com.company.team.lottery.domain.aggregate;

import com.company.team.lottery.domain.entity.LotteryChance;
import com.company.team.lottery.domain.entity.Prize;
import com.company.team.lottery.domain.entity.PrizeCondition;
import com.company.team.lottery.domain.valobj.DrawLotterContext;
import com.company.team.lottery.facade.UserInfoFacade;
import com.order.system.common.VO.UserInfoVO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 奖品限制聚合
 * @author zh
 * @date  2021-01-28
 */
public class AwardCondition {
    @Autowired
    private UserInfoFacade userInfoFacade;

    @Autowired
    private PrizeCondition activityCondition;

    @Autowired
    private LotteryChance lotteryChance;

    /**
     * 主要方法：校验奖品发放权限
     */
    public Prize checkPrizeLimit(List<Prize> prizes){
        return new Prize();
    }
}
