package com.company.team.lottery.domain.aggregate;

import com.company.team.lottery.domain.entity.Prize;
import com.company.team.lottery.domain.valobj.DrawLotterContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 奖品池聚合根
 */
@Component
public class PrizePool {

    @Autowired
    private Prize prize;

    /**
     * 根据概率选择奖池
     *     除了中出的奖品之外还需要有一个安慰奖，后面校验奖品有用到
     * @param lotteryId
     * @return
     */
    public List<Prize> randomGetPrize(DrawLotterContext lotterContext) {
        List<Prize> prizes = prize.getPrizeByLotteryId(lotterContext.getLotteryId());

        /*普通随机数取出*/
        return new ArrayList<Prize>(16);
    }


}
