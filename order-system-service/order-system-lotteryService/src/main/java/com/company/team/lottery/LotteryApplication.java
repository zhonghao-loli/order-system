package com.company.team.lottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@MapperScan(basePackages = "com.order.system.lotteryService.dao")
@ComponentScan(basePackages = {"com.company.team.lottery.controller", "com.company.team.lottery.service", "com.company.team.lottery.repo.dao",
		"com.company.team.lottery.config"})
//@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@EnableTransactionManagement(proxyTargetClass = true)
@EnableEurekaClient
public class LotteryApplication {
	public static void main(String[] args) {
		SpringApplication.run(LotteryApplication.class, args);
	}
}


