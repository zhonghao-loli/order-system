package com.company.team.lottery.domain.entity;


import com.company.team.lottery.repo.PrizeRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽奖奖品,充血模型
 * @author zh
 * @date 2021-01-28
 */
@Component
@Data
public class Prize {
    private Long prizeId;

    private Long lotteryId;

    private Long giftId;

    private String prizeType;

    private String prizeName;

    /**概率*/
    private Double probable;

    @Autowired
    private PrizeRepository prizeRepository;

    public List<Prize> getPrizeByLotteryId(Long lotteryId){
        return prizeRepository.getPrizeByLotteryId(lotteryId);
    }

}
