package com.company.team.lottery.domain.valobj;

import lombok.Data;

/**
 * 抽奖参数键对象
 * @author zh
 * @date 2021-01-28
 */
@Data
public class DrawLotterContext {

    /**抽奖id*/
    private Long lotteryId;

    /**用户id*/
    private Long userId;

    /**抽奖次数来源，如分享，购买商品等*/
    private String chanceSoucre;

}
