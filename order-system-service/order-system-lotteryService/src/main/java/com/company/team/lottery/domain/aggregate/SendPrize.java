package com.company.team.lottery.domain.aggregate;

import com.company.team.lottery.domain.entity.Prize;
import org.springframework.stereotype.Component;

/**
 * 发放奖品聚合
 */
@Component
public class SendPrize {

    // 引入其他奖品相关的微服务，和UserInfoFacade一样，或者可以去掉防腐层相关。

    //

    public void sendPrize(Prize prize){
        // 根据逻辑判断发放，调用其他微服务
    }

}
