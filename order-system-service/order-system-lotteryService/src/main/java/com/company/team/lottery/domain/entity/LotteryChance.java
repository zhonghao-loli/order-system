package com.company.team.lottery.domain.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  活动限制条件实体
 */
@Component
@Data
public class LotteryChance {
    private Long lotteryChanceId;

    private Long activityId;

    /**获取抽奖机会时间*/
    private Date chanceTime;

    /**有效期*/
    private Date expireTime;

    /**抽奖积分来源*/
    private String source;

    public LotteryChance getLotteryChance(Long memberId,String source,Long activityId){
        return new LotteryChance();
    }

    public boolean updateLotteryChance(Long lotteryChanceId){
        return true;
    }


}
