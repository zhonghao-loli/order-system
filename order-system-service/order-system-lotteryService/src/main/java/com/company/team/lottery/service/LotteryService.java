package com.company.team.lottery.service;

import com.company.team.lottery.domain.aggregate.AwardCondition;
import com.company.team.lottery.domain.aggregate.LotteryCondition;
import com.company.team.lottery.domain.aggregate.PrizePool;
import com.company.team.lottery.domain.aggregate.SendPrize;
import com.company.team.lottery.domain.entity.Prize;
import com.company.team.lottery.domain.valobj.DrawLotterContext;
import com.company.team.lottery.facade.RiskServiceFacade;
import com.company.team.lottery.service.impl.ILotteryService;
import com.order.system.common.result.ServiceResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LotteryService implements ILotteryService {
    @Autowired
    private RiskServiceFacade riskServiceFacade;
    @Autowired
    private LotteryCondition lotteryCondition;
    @Autowired
    private AwardCondition awardCondition;
    @Autowired
    private PrizePool prizePool;
    @Autowired
    private SendPrize sendPrize;
    //用户参与抽奖活动
    @Override
    public ServiceResult participateLottery(DrawLotterContext lotterContext) {
        // 校验用户登录信息
        validateLoginInfo(lotterContext);
        // 校验风控
        riskServiceFacade.accquire(lotterContext);

        // 校验活动权限
        lotteryCondition.checkLotteryLimit(lotterContext);

        // 根据概率选择奖池
        List<Prize> prizes = prizePool.randomGetPrize(lotterContext);

        // 校验奖品发放权限
        Prize prize = awardCondition.checkPrizeLimit(prizes);

        // 奖品发放
        sendPrize.sendPrize(prize);

        return ServiceResult.success(prize);
    }



    private void validateLoginInfo(DrawLotterContext lotterContext){
        return;
    }


}
