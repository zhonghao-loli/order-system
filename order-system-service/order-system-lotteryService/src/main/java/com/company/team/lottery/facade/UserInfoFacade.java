package com.company.team.lottery.facade;

import com.company.team.lottery.domain.valobj.DrawLotterContext;
import com.company.team.lottery.repo.client.ConsumerClientService;
import com.google.common.base.Preconditions;
import com.order.system.common.VO.UserInfoVO;
import com.order.system.common.result.ServiceResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserInfoFacade {

    @Autowired
    private ConsumerClientService consumerService;

    public UserInfoVO getUserInfoByUserId(DrawLotterContext dlc){
        ServiceResult userInfo = consumerService.getUserInfo(dlc.getUserId());
        Preconditions.checkArgument(userInfo.getCode() == 200, "获取用户信息失败");
        return UserInfoVO.objectToUserInfoVo(userInfo.getData());
    }

}
