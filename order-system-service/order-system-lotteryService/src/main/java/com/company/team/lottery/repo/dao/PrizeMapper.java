package com.company.team.lottery.repo.dao;

import com.company.team.lottery.domain.entity.Prize;
import com.order.system.common.core.Mapper;

public interface PrizeMapper extends Mapper<Prize> {
}