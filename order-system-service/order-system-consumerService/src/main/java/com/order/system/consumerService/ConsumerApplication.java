package com.order.system.consumerService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "com.order.system.consumerService.dao")
@ComponentScan(basePackages = {"com.order.system.consumerService.controller",
		"com.order.system.consumerService.service", "com.order.system.consumerService.dao",
		"com.order.system.consumerService.config", "com.order.system.consumerService.client"})
// 禁用spring自动配置数据库
//@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableFeignClients(basePackages = "com.order.system.consumerService.client")
@EnableCircuitBreaker
@EnableEurekaClient
public class ConsumerApplication {
	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}
}
