package com.order.system.consumerService.common.query;

import lombok.Data;

@Data
public class TestQuery extends BaseQuery {

    private String test;

}
