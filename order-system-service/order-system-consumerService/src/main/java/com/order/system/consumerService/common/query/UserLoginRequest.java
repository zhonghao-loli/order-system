package com.order.system.consumerService.common.query;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginRequest {
	private String phone;

	private String account;

	@NotBlank(message = "用户密码不能为空")
	private String password;
}
