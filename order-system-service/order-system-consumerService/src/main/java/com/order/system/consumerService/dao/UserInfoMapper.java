package com.order.system.consumerService.dao;

import com.order.system.common.core.Mapper;
import com.order.system.consumerService.model.UserInfo;

public interface UserInfoMapper extends Mapper<UserInfo> {
}