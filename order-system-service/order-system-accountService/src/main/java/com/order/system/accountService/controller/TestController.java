package com.order.system.accountService.controller;

import com.order.system.accountService.common.NoRepeatSubmit;
import com.order.system.common.result.ServiceResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huangzh
 * @ClassName:
 * @date 2021/2/2 16:42
 **/

@RestController
@RequestMapping("test")
@Slf4j
public class TestController {

    @PostMapping("addAccount")
    @ApiOperation(value = "新增用户")
    @NoRepeatSubmit //测试重复提交
    public ServiceResult addAccount(){
        log.info("测试重复提交");
        return ServiceResult.success("");
    }

}
