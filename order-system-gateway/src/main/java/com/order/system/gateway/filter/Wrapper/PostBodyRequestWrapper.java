package com.order.system.gateway.filter.Wrapper;

import com.order.system.gateway.model.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class PostBodyRequestWrapper extends HttpServletRequestWrapper {

   private HttpServletRequest request;
   private  UserInfo userInfo;

    public PostBodyRequestWrapper(HttpServletRequest request,UserInfo userInfo) {
        super(request);
        this.request = request;
        this.userInfo= userInfo;
    }

    public PostBodyRequestWrapper(HttpServletRequest request) {
        super(request);
    }
}
